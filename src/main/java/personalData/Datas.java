package personalData;

public class Datas {

    public static final String MY_EMAIL = "testmail@email.com";
    public static final String MY_PASSWORD = "testpassword123";
    public static final String TITLE = "Mr";
    public static final String FIRST_NAME = "Nikola";
    public static final String LAST_NAME = "Amedovic";
    public static final String DAY = "12";
    public static final String MONTH = "3";
    public static final String YEAR = "1984";
    public static final String COMPANY = "Big Company";
    public static final String ADDRESS = "221B Baker Street";
    public static final String ADDRESS2 = "address2";
    public static final String CITY = "Example City";
    public static final String STATE = "Kansas";
    public static final String ZIP_CODE = "12345";
    public static final String ADDITIONAL_INFO = "Some additional informations";
    public static final String HOME_PHONE_NUMBER = "222-333";
    public static final String MOBILE_PHONE_NUMBER = "555-666";
    public static final String ADDRESS_ALIAS = "Shipping address";

}
