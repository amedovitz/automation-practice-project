package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By BANK_WIRE = By.className("bankwire");
    public static final By CHECK = By.className("cheque");
    public static final By CONFIRM_BTN = By.xpath("//*[@id=\"cart_navigation\"]/button");
    public static final By ORDER_COMPLETE_MSG = By.xpath("//*[@id=\"center_column\"]/p[1]");

    public PaymentPage(WebDriver driver, WebDriverWait waiter){
        this.driver = driver;
        this.waiter = waiter;
    }

    public void paymentMethodSelect(String payment){
        if(payment.equalsIgnoreCase("Bank wire")){
            driver.findElement(BANK_WIRE).click();
        }else if(payment.equalsIgnoreCase("Check")){
            driver.findElement(CHECK).click();
        }
    }

    public void confirmOrder(){
        WebElement confirmBtn = driver.findElement(CONFIRM_BTN);
        waiter.until(ExpectedConditions.elementToBeClickable(confirmBtn)).click();
    }

    public boolean isDisplayed(){
        try {
            driver.findElement(ORDER_COMPLETE_MSG);
            return true;
        } catch (Exception e){
            return false;
        }
    }

}
