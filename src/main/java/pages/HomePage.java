package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    public WebDriver driver;
    public static final By signIn = By.className("login");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public void goToSignIn(){
        driver.findElement(signIn).click();
    }


}
