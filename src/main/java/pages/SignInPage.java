package pages;

import personalData.Datas;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By EMAIL = By.id("email");
    public static final By PASSWORD = By.id("passwd");
    public static final By SIGN_IN_BTN = By.id("SubmitLogin");
    public static final By ERROR_MSG = By.xpath("//*[@id=\"center_column\"]/div[1]");
    public static final By MY_ACCOUNT = By.className("account");

    public SignInPage(WebDriver driver, WebDriverWait waiter){
        this.driver = driver;
        this.waiter = waiter;
    }

    public void setEmail(String myEmail){
        driver.findElement(EMAIL).sendKeys(myEmail);
    }

    public void setPassword(String myPassword){
        driver.findElement(PASSWORD).sendKeys(myPassword);
    }

    public void logIn(String myEmail, String myPassword){
        this.setEmail(myEmail);
        this.setPassword(myPassword);
        driver.findElement(SIGN_IN_BTN).click();
    }

    public void noPasswordLogin(String myEmail){
        this.setEmail(myEmail);
        driver.findElement(SIGN_IN_BTN).click();
    }

    public boolean isNotLoggedIn(){
        try {
            this.waiter.until(ExpectedConditions.visibilityOfElementLocated(ERROR_MSG));
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean isLoggedIn(){
        try {
            this.driver.findElement(MY_ACCOUNT).getText().contains(Datas.FIRST_NAME);
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
