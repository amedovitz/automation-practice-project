package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class CreateAnAccountPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By EMAIL = By.id("email_create");
    public static final By CREATE_ACCOUNT_BTN = By.id("SubmitCreate");
    public static final By ERROR_MSG = By.id("create_account_error");

    public CreateAnAccountPage(WebDriver driver, WebDriverWait waiter){
        this.driver = driver;
        this.waiter = waiter;
    }

    public void setEmail(String myEmail){
        driver.findElement(EMAIL).sendKeys(myEmail);
    }

    public void creatingAnAccount(String email){
        this.setEmail(email);
        driver.findElement(CREATE_ACCOUNT_BTN).click();
    }

    public String generateRandomEmail() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int length = 7;

        for(int i = 0; i < length; i++) {
            int index = random.nextInt(alphabet.length());
            char randomChar = alphabet.charAt(index);
            sb.append(randomChar);
        }
        String randomString = sb.toString() + "@mail.com";
        return randomString;
    }

    public boolean isOnPersonalInfoPage(){
        try {
            driver.getCurrentUrl().contains("account-creation");
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean isNotOnPersonalInfoPage(){
        try {
            waiter.until(ExpectedConditions.presenceOfElementLocated(ERROR_MSG));
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
