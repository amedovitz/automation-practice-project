package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyAccountPage {

    public WebDriver driver;
    public static final By WOMEN = By.xpath("//*[@id=\"block_top_menu\"]/ul/li[1]/a");

    public MyAccountPage(WebDriver driver){
        this.driver = driver;
    }

    public void navigateToWomenSection(){
        driver.findElement(WOMEN).click();
    }

}
