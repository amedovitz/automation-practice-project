package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShippingPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By I_AGREE_BTN = By.id("cgv");
    public static final By PROCEED_TO_CHECKOUT_BTN = By.name("processCarrier");

    public ShippingPage(WebDriver driver){
        this.driver = driver;
    }

    public void iAgreeCheck(){
        driver.findElement(I_AGREE_BTN).click();
    }

    public void proceedToCheckout(){
        driver.findElement(PROCEED_TO_CHECKOUT_BTN).click();
    }

}
