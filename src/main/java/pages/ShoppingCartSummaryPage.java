package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShoppingCartSummaryPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By PROCEED_TO_CHECKOUT_BTN = By.className("standard-checkout");

    public ShoppingCartSummaryPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement checkoutBtn() {
         return driver.findElement(PROCEED_TO_CHECKOUT_BTN);
    }

    public void scrollAndClick(WebElement element) {
        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].scrollIntoView();", element);
            waiter.until(ExpectedConditions.elementToBeClickable(element));
            jse.executeScript("arguments[0].click();", element);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }element.click();
    }
}
