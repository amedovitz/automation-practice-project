package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WomenSectionPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By BLOUSE = By.xpath("//*[@id=\"center_column\"]/ul/li[2]/div");
    public static final By ADD_TO_CART_BTN = By.xpath("//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/div[2]/a[1]");
    public static final By PROCEED_BTN = By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a");

    public WomenSectionPage(WebDriver driver, WebDriverWait waiter){
        this.driver = driver;
        this.waiter = waiter;
    }

    public void addToCart(){
        WebElement blouseElement = driver.findElement(BLOUSE);
        Actions action = new Actions(driver);
        action.moveToElement(blouseElement).build().perform();
        driver.findElement(ADD_TO_CART_BTN).click();
    }

    public void proceedToCheckout(){
        WebElement proceedToCheckoutBtn = driver.findElement(PROCEED_BTN);
        waiter.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutBtn)).click();
    }

}
