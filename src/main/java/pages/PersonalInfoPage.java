package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PersonalInfoPage {

    public WebDriver driver;
    public WebDriverWait waiter;
    public static final By mrTitle = By.id("id_gender1");
    public static final By mrsTitle = By.id("id_gender2");
    public static final By firstName = By.id("customer_firstname");
    public static final By lastName = By.id("customer_lastname");
    public static final By password = By.id("passwd");
    public static final By dayOfBirth = By.id("days");
    public static final By monthOfBirth = By.id("months");
    public static final By yearOfBirth = By.id("years");
    public static final By newsletters = By.id("newsletter");
    public static final By specialOffers = By.id("optin");
//    public static final By addressFirstName = By.id("firstname");
//    public static final By addressLastName = By.id("lastname");
    public static final By company = By.id("company");
    public static final By address = By.id("address1");
    public static final By address2 = By.id("address2");
    public static final By city = By.id("city");
    public static final By state = By.id("id_state");
    public static final By zipCode = By.id("postcode");
//    public static final By country = By.id("id_country");
    public static final By additionalInfo = By.id("other");
    public static final By homePhone = By.id("phone");
    public static final By mobilePhone = By.id("phone_mobile");
    public static final By addressAlias = By.id("alias");
    public static final By registerBtn = By.id("submitAccount");
    public static final By signOut = By.className("logout");

    public PersonalInfoPage(WebDriver driver, WebDriverWait waiter){
        this.driver = driver;
        this.waiter = waiter;
    }

    public void setTitle(String title){
        if(title.equalsIgnoreCase("Mr"))
            driver.findElement(mrTitle).click();
        else if(title.equalsIgnoreCase("Mrs"))
            driver.findElement(mrsTitle).click();
    }

    public void setFirstName(String name){
        driver.findElement(firstName).sendKeys(name);
    }

    public void setLastName(String surName){
        driver.findElement(lastName).sendKeys(surName);
    }

    public void setPassword(String pswd){
        driver.findElement(password).sendKeys(pswd);
    }

    public void setDayOfBirth(String day){
        WebElement yourBirthDay = driver.findElement(dayOfBirth);
        yourBirthDay.click();
        Select days = new Select(yourBirthDay);
        days.selectByValue(day);
    }

    public void setMonthOfBirth(String month){
        WebElement yourBirthMonth = driver.findElement(monthOfBirth);
        yourBirthMonth.click();
        Select days = new Select(yourBirthMonth);
        days.selectByValue(month);
    }

    public void setYearOfBirth(String year){
        WebElement yourBirthYear = driver.findElement(yearOfBirth);
        yourBirthYear.click();
        Select days = new Select(yourBirthYear);
        days.selectByValue(year);
    }

    public void setNewsletters(){
        driver.findElement(newsletters).click();
    }

    public void setSpecialOffers(){
        driver.findElement(specialOffers).click();
    }

    public void setCompany(String companyName){
        driver.findElement(company).sendKeys(companyName);
    }

    public void setAddress(String address1){
        driver.findElement(address).sendKeys(address1);
    }

    public void setAddress2(String secondAddress){
        driver.findElement(address2).sendKeys(secondAddress);
    }

    public void setCity(String yourCity){
        driver.findElement(city).sendKeys(yourCity);
    }

    public void setState(String yourState){
        WebElement someState = driver.findElement(state);
        someState.click();
        Select states = new Select(someState);
        states.selectByVisibleText(yourState);
    }

    public void setZipCode(String postalCode){
        driver.findElement(zipCode).sendKeys(postalCode);
    }

    public void setAdditionalInfo(String addInfo){
        driver.findElement(additionalInfo).sendKeys(addInfo);
    }

    public void setHomePhone(String homePhoneNumber){
        driver.findElement(homePhone).sendKeys(homePhoneNumber);
    }

    public void setMobilePhone(String phoneNumber){
        driver.findElement(mobilePhone).sendKeys(phoneNumber);
    }

    public void setAddressAlias(String alias){
        driver.findElement(addressAlias).clear();
        driver.findElement(addressAlias).sendKeys(alias);
    }

    public void submitForm(){
        driver.findElement(registerBtn).click();
    }

    public void fillAllFields(String title, String name, String surName, String pswd, String day, String month,
                              String year,String company, String address1, String address2, String yourCity,
                              String yourState, String postalCode, String additionalInfo,String homePhoneNumber,
                              String phoneNumber, String alias){
        this.setTitle(title);
        this.setFirstName(name);
        this.setLastName(surName);
        this.setPassword(pswd);
        this.setDayOfBirth(day);
        this.setMonthOfBirth(month);
        this.setYearOfBirth(year);
        this.setNewsletters();
        this.setSpecialOffers();
        this.setCompany(company);
        this.setAddress(address1);
        this.setAddress2(address2);
        this.setCity(yourCity);
        this.setState(yourState);
        this.setZipCode(postalCode);
        this.setAdditionalInfo(additionalInfo);
        this.setHomePhone(homePhoneNumber);
        this.setMobilePhone(phoneNumber);
        this.setAddressAlias(alias);
        this.submitForm();
    }

    public void fillMandatoryFields(String name, String surName, String pswd, String address1, String yourCity, String yourState,
                         String postalCode, String phoneNumber, String alias){

        this.setFirstName(name);
        this.setLastName(surName);
        this.setPassword(pswd);
        this.setAddress(address1);
        this.setCity(yourCity);
        this.setState(yourState);
        this.setZipCode(postalCode);
        this.setMobilePhone(phoneNumber);
        this.setAddressAlias(alias);
        this.submitForm();
    }

    public boolean isRegistered() {
        try {
            waiter.until(ExpectedConditions.presenceOfElementLocated(signOut));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
