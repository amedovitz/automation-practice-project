import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BaseTest {

    public WebDriver driver;
    public WebDriverWait waiter;
    public Properties locators;
    HomePage homePage;
    CreateAnAccountPage createAnAccountPage;
    PersonalInfoPage personalInfoPage;
    SignInPage signInPage;
    MyAccountPage myAccountPage;
    WomenSectionPage womenSectionPage;
    ShoppingCartSummaryPage shoppingCartSummaryPage;
    AddressPage addressPage;
    ShippingPage shippingPage;
    PaymentPage paymentPage;

    public WebDriver creatingAWebDriver() throws IOException {
        locators = new Properties();
        locators.load(new FileInputStream("configuration.properties"));
        String browser = locators.getProperty("browser");

        switch (browser){
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case "edge":
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
        }
        return driver;
    }

    @BeforeMethod
    public void setUpForTest() throws IOException {
        driver = this.creatingAWebDriver();
        waiter = new WebDriverWait(driver, 60);
        driver.manage().window().maximize();
        driver.navigate().to(locators.getProperty("homePageUrl"));
        homePage = new HomePage(driver);
        createAnAccountPage = new CreateAnAccountPage(driver, waiter);
        personalInfoPage = new PersonalInfoPage(driver, waiter);
        signInPage = new SignInPage(driver, waiter);
        myAccountPage = new MyAccountPage(driver);
        womenSectionPage = new WomenSectionPage(driver, waiter);
        shoppingCartSummaryPage = new ShoppingCartSummaryPage(driver);
        addressPage = new AddressPage(driver);
        shippingPage = new ShippingPage(driver);
        paymentPage = new PaymentPage(driver, waiter);
    }

    @AfterMethod
    public void closingBrowser(){
        driver.close();
    }

}
