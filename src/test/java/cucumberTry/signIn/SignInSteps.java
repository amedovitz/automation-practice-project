package cucumberTry.signIn;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import personalData.Datas;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SignInSteps {

    public WebDriver driver;
    public Properties locators;
    @Before
    public void setUp() throws IOException {
        driver = new ChromeDriver();
        locators = new Properties();
        locators.load(new FileInputStream("configuration.properties"));
    }
    @Given("Registered user on authentication page")
    public void theUserIsOnAuthenticationPage(){
        driver.navigate().to(locators.getProperty("authenticationPageUrl"));
    }
    @When("User enter an email")
    public void userEnterEmail(){
        driver.findElement(By.id("email")).sendKeys(Datas.MY_EMAIL);
    }
    @And("user enter the password")
    public void userEnterPassword(){
        driver.findElement(By.id("passwd")).sendKeys(Datas.MY_PASSWORD);
    }
    @And("user click on sign in button")
    public void clickOnSignIn(){
        driver.findElement(By.id("SubmitLogin")).click();
    }
    @Then("user should be redirected to my account page")
    public void userIsSignedIn(){
        WebElement myName = driver.findElement(By.className("account"));
        Assert.assertTrue(myName.getText().contains(Datas.FIRST_NAME));
    }
    @After
    public void close(){
        driver.close();
    }
}
