Feature: Login functionality
Scenario: Valid login
Given Registered user on authentication page
When User enter an email
And user enter the password
And user click on sign in button
Then user should be redirected to my account page
