package cucumberTry.itemOrders;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import personalData.Datas;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MultipleItemOrder {

    public WebDriver driver;
    public Properties locators;
    public WebDriverWait waiter;
    @Before
    public void setUp() throws IOException {
        driver = new ChromeDriver();
        waiter = new WebDriverWait(driver, 60);
        locators = new Properties();
        locators.load(new FileInputStream("configuration.properties"));
        driver.manage().window().maximize();
    }
    @Given("Registered user is on authentication page")
    public void theUserIsOnAuthenticationPage(){
        driver.navigate().to(locators.getProperty("authenticationPageUrl"));
    }
    @When("User enters an email")
    public void userEnterEmail(){
        driver.findElement(By.id("email")).sendKeys(Datas.MY_EMAIL);
    }
    @And("user enters the password")
    public void userEnterPassword(){
        driver.findElement(By.id("passwd")).sendKeys(Datas.MY_PASSWORD);
    }
    @And("user click on sign in button,")
    public void clickOnSignIn(){
        driver.findElement(By.id("SubmitLogin")).click();
    }
    @Then("user should be redirected to my account page.")
    public void isSignedIn(){
        WebElement myName = driver.findElement(By.className("account"));
        Assert.assertTrue(myName.getText().contains(Datas.FIRST_NAME));
    }
    @When("user click on women button")
    public void navigateToWomenSection(){
        driver.findElement(By.xpath(locators.getProperty("womenSection"))).click();
    }
    @Then("women page should open")
    public void isOnWomenPage(){
        WebElement womenMsg = driver.findElement(By.xpath(locators.getProperty("womenMsg")));
        Assert.assertEquals(womenMsg.getText(), "Women");
    }
    @When("user add short sleeve T-shirt to cart")
    public void addingTShirtToCart(){
        WebElement tShirt = driver.findElement(By.xpath(locators.getProperty("tShirt")));
        Actions action = new Actions(driver);
        action.moveToElement(tShirt).build().perform();
        driver.findElement(By.xpath(locators.getProperty("tShirtBtn"))).click();
        WebElement continueToShoppingBtn = driver.findElement(By.xpath(locators.getProperty("continueToShoppingBtn")));
        waiter.until(ExpectedConditions.elementToBeClickable(continueToShoppingBtn)).click();
    }
    @And("add blouse to cart")
    public void addingBlouseToCart(){
        WebElement blouse = driver.findElement(By.xpath(locators.getProperty("blouse")));
        Actions action = new Actions(driver);
        action.moveToElement(blouse).build().perform();
        driver.findElement(By.xpath(locators.getProperty("blouseBtn"))).click();
        WebElement continueToShoppingBtn = driver.findElement(By.xpath(locators.getProperty("continueToShoppingBtn")));
        waiter.until(ExpectedConditions.elementToBeClickable(continueToShoppingBtn)).click();
    }
    @And("add printed dress to cart")
    public void addingPrintedDressToCart(){
        WebElement printedDress = driver.findElement(By.xpath(locators.getProperty("printedDress")));
        Actions action = new Actions(driver);
        action.moveToElement(printedDress).build().perform();
        driver.findElement(By.xpath(locators.getProperty("printedDressBtn"))).click();
        WebElement continueToShoppingBtn = driver.findElement(By.xpath(locators.getProperty("continueToShoppingBtn")));
        waiter.until(ExpectedConditions.elementToBeClickable(continueToShoppingBtn)).click();
    }
    @And("add printed summer dress to cart")
    public void addingPrintedSummerDressToCart(){
        WebElement printedDress = driver.findElement(By.xpath(locators.getProperty("printedSummerDress")));
        Actions action = new Actions(driver);
        action.moveToElement(printedDress).build().perform();
        driver.findElement(By.xpath(locators.getProperty("printedSummerDressBtn"))).click();
        WebElement continueToShoppingBtn = driver.findElement(By.xpath(locators.getProperty("continueToShoppingBtn")));
        waiter.until(ExpectedConditions.elementToBeClickable(continueToShoppingBtn)).click();
    }
    @And("add printed chiffon dress to cart")
    public void addingPrintedChiffonDressToCart(){
        WebElement printedDress = driver.findElement(By.xpath(locators.getProperty("printedChiffonDress")));
        Actions action = new Actions(driver);
        action.moveToElement(printedDress).build().perform();
        driver.findElement(By.xpath(locators.getProperty("printedChiffonDressBtn"))).click();
        WebElement continueToShoppingBtn = driver.findElement(By.xpath(locators.getProperty("continueToShoppingBtn")));
        waiter.until(ExpectedConditions.elementToBeClickable(continueToShoppingBtn)).click();
    }
    @Then("five products should be in the cart")
    public void quantityCheck(){
        WebElement cartQuantity = driver.findElement(By.xpath(locators.getProperty("cartQuantity")));
        Assert.assertEquals(cartQuantity.getText(), "5");
    }
    @After
    public void close(){
        driver.close();
    }
}
