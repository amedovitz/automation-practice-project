Feature: Multiple items order
  As registered user,
  I want to order multiple items
Scenario: Five items order
Given Registered user is on authentication page
When User enters an email
And user enters the password
And user click on sign in button,
Then user should be redirected to my account page.
When user click on women button
Then women page should open
When user add short sleeve T-shirt to cart
And add blouse to cart
And add printed dress to cart
And add printed summer dress to cart
And add printed chiffon dress to cart
Then five products should be in the cart