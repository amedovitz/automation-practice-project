import personalData.Datas;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTest extends BaseTest{

    @Test
    public void validDataLogIn(){
        homePage.goToSignIn();
        signInPage.logIn(Datas.MY_EMAIL, Datas.MY_PASSWORD);

        Assert.assertTrue(signInPage.isLoggedIn(), "Ooops, something went wrong.");
    }

    @Test
    public void noPasswordLogIn(){
        homePage.goToSignIn();
        signInPage.noPasswordLogin(Datas.MY_EMAIL);

        Assert.assertTrue(signInPage.isNotLoggedIn());
    }
}
