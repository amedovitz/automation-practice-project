import personalData.Datas;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckoutTest extends BaseTest{

    @Test
    public void completeOrderProcess(){
        homePage.goToSignIn();
        signInPage.logIn(Datas.MY_EMAIL, Datas.MY_PASSWORD);
        myAccountPage.navigateToWomenSection();
        womenSectionPage.addToCart();
        womenSectionPage.proceedToCheckout();
        shoppingCartSummaryPage.scrollAndClick(shoppingCartSummaryPage.checkoutBtn());
        addressPage.scrollAndClick(addressPage.checkoutBtn());
        shippingPage.iAgreeCheck();
        shippingPage.proceedToCheckout();
        paymentPage.paymentMethodSelect("Check");
        paymentPage.confirmOrder();

        Assert.assertTrue(paymentPage.isDisplayed());
    }

}
