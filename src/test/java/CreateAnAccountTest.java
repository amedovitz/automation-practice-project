import personalData.Datas;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateAnAccountTest extends BaseTest{

    @Test
    public void validDataCreation(){
        homePage.goToSignIn();
        createAnAccountPage.creatingAnAccount(createAnAccountPage.generateRandomEmail());

        Assert.assertTrue(createAnAccountPage.isOnPersonalInfoPage());
    }

    @Test
    public void existingEmailCreation(){
        homePage.goToSignIn();
        createAnAccountPage.creatingAnAccount(Datas.MY_EMAIL);

        Assert.assertTrue(createAnAccountPage.isNotOnPersonalInfoPage(), "Wrong page!");
    }
}
