import personalData.Datas;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PersonalInfoTest extends BaseTest{

    @Test
    public void mandatoryFieldsRegistration(){
        driver.navigate().to(locators.getProperty("authenticationPageUrl"));
        createAnAccountPage.creatingAnAccount(createAnAccountPage.generateRandomEmail());
        waiter.until(ExpectedConditions.urlToBe(locators.getProperty("formPageUrl")));
        personalInfoPage.fillMandatoryFields(Datas.FIRST_NAME, Datas.LAST_NAME, Datas.MY_PASSWORD, Datas.ADDRESS,
                Datas.CITY, Datas.STATE, Datas.ZIP_CODE, Datas.MOBILE_PHONE_NUMBER, Datas.ADDRESS_ALIAS);

        Assert.assertTrue(personalInfoPage.isRegistered(), "Unsuccessful registration");
    }

    @Test
    public void allFieldsRegistration(){
        driver.navigate().to(locators.getProperty("authenticationPageUrl"));
        createAnAccountPage.creatingAnAccount(createAnAccountPage.generateRandomEmail());
        waiter.until(ExpectedConditions.urlToBe(locators.getProperty("formPageUrl")));
        personalInfoPage.fillAllFields(Datas.TITLE, Datas.FIRST_NAME, Datas.LAST_NAME, Datas.MY_PASSWORD, Datas.DAY,
                Datas.MONTH, Datas.YEAR, Datas.COMPANY, Datas.ADDRESS, Datas.ADDRESS2, Datas.CITY, Datas.STATE, Datas.ZIP_CODE,
                Datas.ADDITIONAL_INFO, Datas.HOME_PHONE_NUMBER, Datas.MOBILE_PHONE_NUMBER, Datas.ADDRESS_ALIAS);

        Assert.assertTrue(personalInfoPage.isRegistered(), "Unsuccessful registration");
    }
}
